version_info = (3, 0, 0, 'pre1')
version = '.'.join(str(n) for n in version_info[:3])
release = '.'.join(str(n) for n in version_info)
